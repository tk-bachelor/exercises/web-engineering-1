window.addEventListener("load", function(){

    // register mouseover events
    var btnChangeMe = document.getElementById("changeMe");
    btnChangeMe.addEventListener('mouseenter', function(){
        btnChangeMe.setAttribute('type', 'text');
    });

    btnChangeMe.addEventListener('mouseleave', function(){
        btnChangeMe.setAttribute('type', 'button');
    });
});
