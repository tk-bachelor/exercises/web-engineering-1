# Web Engineering + Design 1

## Week 1: 24.Feb 2016 - HTML Introduction
- HTML Basics
- Encoding and HTML Markup validation

## Week 2: 02.Mar 2016 - HTML Basics
- HTML DocType
- Content Model
- Browser support - Can I Use?

## Week 3: 09.Mar 2016 - HTML Advanced
- Tables, Images, Figure
- Multimedia, Active content, Forms
- Accessibility

## Week 5: 23.Mar 2016 - CSS
- CSS Selectors
- CSS Styling

## Week 6: 30.Mar 2016 - CSS Advanced
- CSS attr tooltip
- CSS navigation
- CSS popup

## Week 7: 06.Apr 2016 - JavaScript Introduction
- JS Expression
- JS Testing
- JS Types
- JS Functions

## Week 8: 20.Apr 2016 - JavaScript Advanced
- JS Scopes
- Context
- Functions & Overloading
- SetTimeout & Hoisting

## Week 9: 27.Apr 2016 - DOM Events
- Register Events
- Currency Converter
- Event Handling & DOM Manipulation
- Copy

## Week 10: 04.May 2016 - JQuery