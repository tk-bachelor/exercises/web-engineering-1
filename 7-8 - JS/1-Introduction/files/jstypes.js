/**
 * Created by Keerthikan on 06-Apr-16.
 */

function main() {
    console.log(a, typeof a); // value:         type:               reason:
    var a = 5.5;
    console.log(a, typeof a); // value:         type:               reason:

    var b = parseInt(a);
    console.log(b, typeof b); // value:         type:               reason:

    var c = b + "5";
    console.log(c, typeof c); // value:         type:               reason:

    var d = "5" + b;
    console.log(d, typeof d); // value:         type:               reason:

    var e = b + b + "auto";
    console.log(e, typeof e); // value:         type:               reason:

    var f = b - "3";
    console.log(f, typeof f); // value:         type:               reason:

    var g = b - "a";
    console.log(g, typeof g); // value:         type:               reason:

    var h = "abc" < "def";
    console.log(h, typeof h); // value:         type:               reason:

    var i = [];
    console.log(i, typeof i); // value:         type:               reason:

    var j = {};
    console.log(j, typeof j); // value:         type:               reason:

    console.log(i == "");    // value:                              reason:

    console.log(j == "");    // value:                              reason:

    console.log(i === "");     // value:                            reason:

    var k = function() { return "a"; }
    console.log(k, typeof k); // value:         type:               reason:

    var l = main;
    console.log(l, typeof l); // value:         type:               reason:

    var m = NaN;
    console.log(m, typeof m); // value:         type:               reason:

    var n = null;
    console.log(n, typeof n); // value:         type:               reason:

    var o = /hsr/i;
    console.log(o, typeof o); // value:         type:               reason:

    console.log(typeof undefined);     // value:                    reason:

    console.log(null === undefined);   // value:                    reason:

    console.log(null == undefined);    // value:                    reason:
}
main();
