/**
 * Created by Keerthikan on 09-Apr-16.
 */

function isArray(array){
    if(typeof array === 'object'){
        if(array instanceof Array)
            return true;
    }
    return false;
}

var a = [];
var b = 5;
var c = "abc";
var d = [2,3];
var e = {"a":5, "b": 6};

console.log("isArray");
console.log("===================");
console.log(isArray(a));
console.log(isArray(b));
console.log(isArray(c));
console.log(isArray(d));
console.log(isArray(e));

console.log("Palindrome");
console.log("===================");

function isPalindrome(word){
    if(typeof word !== "string")
        return false;

    var length = word.length;
    for(var i = 0; i < length / 2; i++){
        if(word[i] !== word[length-i-1]){
            return false;
        }
    }
    return true;
}

function isPalindromeReverse(word){
    var array = word.split("");
    var reversed = array.reverse();
    var joined = reversed.join("");
    return word === joined;
}

function isPalindormeRecursive(word){
    console.log(word);
    if(word === "" || word.length == 1){
        return true;
    }else{
        if(word[0] !== word[word.length - 1]){
            return false;
        }else{
            return isPalindormeRecursive(word.slice(1,word.length-1));
        }
    }
}

var aa = 454;
var ab = [2,3,3,2];
var ac = "amma";
var ba = 123;
var bb = [1,2,3,4];
var bc = "abcnbca";

console.log(isPalindrome(aa));
console.log(isPalindrome(ab));
console.log(isPalindrome(ac));
console.log(isPalindrome(ba));
console.log(isPalindrome(bb));
console.log(isPalindrome(bc));

console.log("==");

console.log(isPalindromeReverse(ac));
console.log(isPalindromeReverse(bc));

console.log("Recursive");
console.log(isPalindormeRecursive(ac));
console.log(isPalindormeRecursive(bc));

console.log("\n");
console.log("Show Data");
console.log("==\n");

function showData(obj){
    Object.keys(obj).forEach(function(key){
        console.log(key, obj[key]);
    })
}

var obj1 = {a: 5, b: 24, c: "abc"};
var obj2 = {a: 5, b: {ba: 21, bb: 22}, c: [2,3]};
showData(obj1);
showData(obj2);