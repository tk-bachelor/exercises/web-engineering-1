/**
 * Created by Keerthikan on 26-Apr-16.
 */
function clock(seconds, func){
    setTimeout(func, seconds*1000);
    console.log(`timer goes off in: ${seconds} seconds`);
}

clock(3, () => console.log("Hello World"));