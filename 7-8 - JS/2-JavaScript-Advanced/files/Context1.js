counter = 999;
var object = { counter:3 , foo : function(){ console.log( this.counter)}};
var object2 = {counter : 100};
var  foo = object.foo;

object.foo();  // 3
foo();          // 999
foo.apply({counter : 10});  //10

object2.foo = foo;
object2.foo(); // 100


var newFoo = foo.bind({counter : 11});
newFoo(); // 11
newFoo.apply({counter : 12}); // cannot be modified

new foo(); // creates new object
            // the constructor will also called
            // in this case console.log(this.counter)

// bind wird ignoriert
new newFoo(); // undefined