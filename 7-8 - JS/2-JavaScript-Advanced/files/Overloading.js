'use strict'

function say(text, count) {
	//console.log(count);
	//count = count || 1;
	var output = "";
	for (var i = 0; i < count; ++i) {
		output += " " + text;
	}
	console.log("I say " + output);
}


say("hi", 3);
say("hi");

// Java: Erwartet I say hi hi hi
// JS  : Outcome
