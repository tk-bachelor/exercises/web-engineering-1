function counter(start){
    return function(){
       return start++;
    }
}

var c1 = counter(2);
// start = 2;
// function (){
//      return start++;
// }

console.log(c1());  // 2
console.log(c1());  // 3
console.log(c1());  // 4


var c2 = counter(-10); // start = -10
console.log(c2()); // -10
console.log(c2()); // -9
console.log(c2()); // -8