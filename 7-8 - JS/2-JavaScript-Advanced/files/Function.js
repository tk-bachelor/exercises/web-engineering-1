function sayHi(where){
    try{
        where("hi");
    }
    catch(e){
        console.log("error");
    }
}


//(x)=>console.log(x)
// function (x){ console.log(x);
sayHi((x)=>console.log(x)); // hi
sayHi(console.log);         // hi
sayHi(console.log());       // error
sayHi((x)=>console.log(1,x)); // hi
sayHi(console.log); // hi mit rot
sayHi(console.log()); // error

console.fun = function(){
    this.log("haha");
};

console.fun(); // haha
sayHi(console.fun); // this = global, therefore global.log existiert nicht