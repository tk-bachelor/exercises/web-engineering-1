function getTime(onTimeAvailable, onTimeError, expectedFormat, method) {
    if (typeof(onTimeAvailable) !== 'function') {
        throw "'onTimeAvaiable' must be a function";
    }
    if (typeof(onTimeError) !== 'function') {
        throw "'onTimeError' must be a function";
    }

    //var client = new XMLHttpRequest();
    //client.onreadystatechange = function () {
    //    if (client.readyState === 4) {
    //        if (client.status === 200) {
    //            onTimeAvailable(client.responseText);
    //        }
    //        else {
    //            onTimeError(client);
    //        }
    //    }
    //};
    //
    //client.onerror = onTimeError;
    //client.open('GET', '/time', true);
    //client.send(JSON.stringify({zone: "+4"}));

    //$.get("/time").done(onTimeAvailable).fail(onTimeError);

    if(method == "get") {
        // GET method
        $.ajax({
            url: "/time",
            method: "get",
            cache: false,
            dataType: expectedFormat   // expected data format
        }).done(onTimeAvailable)
            .error(onTimeError);

    }else {
        $.ajax({
            url: "/time",
            method: "post",
            dataType: expectedFormat,
            data: JSON.stringify({zone: "+4"}),
            contentType: "application/json"
        }).done(onTimeAvailable)
            .error(onTimeError);
    }

}