Time-API
========

Requirements
------------
* npm/node

Installation
------------

```
cd "server"
npm install
```

Usage
-----

* run `npm start` to start the application
* Open `http://localhost:8080/` in your browser
* Call `http://localhost:8080/time` to get the current time
* Call `http://localhost:8080/time?format=json` to get the current time in JSON format
