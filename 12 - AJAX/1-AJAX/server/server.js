/**
 * Server configuration
 */
var express = require('express');
var bodyParser = require('body-parser');
var url = require("url");

var allowCrossDomain = function(request, response, next) {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Methods', 'GET');
    response.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

/**
 * Basic server
 */
var app = express();
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use('/', express.static(__dirname + '/app'));

/**
 * API routes
 */
app.get('/time', function (request, response) {
	var time = new Date();
	response.format({
		'text/html': function () {
			response.contentType('text/plain');
			response.send(time.toString());
		},
		'application/json': function () {
			response.contentType('application/json');
			response.send(JSON.stringify({time: time}));
		}
	});
});

app.post('/time', function (request, response) {
	console.log("zone", request.body);
	var time = new Date();

	if(request.body.zone) {
		time = new Date(time.setHours(time.getHours() + parseInt(request.body.zone)));
	}

	response.format({
		'text/html': function () {
			response.contentType('text/plain');
			response.send(time.toString());
		},
		'application/json': function () {
			response.contentType('application/json');
			response.send(JSON.stringify({time: time}));
		}
	});
});


/**
 * Server start
 */
var appPort = 8080;
app.listen(appPort);
console.log('Server running on http://localhost:'+appPort);